---
marp: true
theme: gaia
title: OAW2024-atelier.md
author: Daphné Mathelier (MSH Mondes)
paginate: true
backgroundImage: "url('icono/fond-OAW.png')"
footer: "Semaine du libre accès (Université Paris Nanterre) –  **Concevoir un site statique** – 23/10/2024 – Daphné Mathelier (Projet STRAD, MSH Mondes)"
---

<style> 
a {text-decoration: none; color: #f37d21;}
blockquote {background: #F5F5F5; padding: 0.75em; margin: 1em; color: ##8da09c; font-size: 23.5px;}
blockquote:before, blockquote:after {content: unset;}
code {color: #0B5345; background: none;}
footer {font-size: 14px; text-align:center;} # par défaut alignement à gauche
h1 {font-size: 64px; color: #8da09c;}
h2 {color: #f37d21;}
h3 {margin-bottom: 20px; color: #737987;}
h4 {margin-top: 20px; margin-bottom: 20px; color: #E67E22;}
img[alt~="center"] {display: block; margin-top: 20px;}
section {font-size: 26px; color:F737987}
table {font-size: 24px;}
ul {list-style-type: none; margin-top: -1px; margin-bottom: -1px;}
ul > li:before {content: "–"; position: absolute; margin-left: -1em;}
</style>

<style scoped> footer{font-size : 0px;} img{margin-right: -40px;} </style> 

<!-- PAD : https://pad.numerique.gouv.fr/n92l--I-RlWiacZkp05iQg?both -->

# Concevoir un site statique <!-- paginate: false --> <img align="right" width=" 450" src="icono/logo-OAW.png">
## OAW 2024 – 23 octobre 2024 
</br> 

**Daphné MATHELIER** | [daphne.mathelier@cnrs.fr](mailto:daphne.mathelier@cnrs.fr)
 

- Pôle Données et Humanités numériques (**MSH Mondes**)
- Correspondante de l’IR* **Huma-Num**
- Membre de l’atelier de la donnée de Nanterre (**ADN**)
- Co-coordinatrice du projet **STRAD** (Stratégies pour l’archivage durable)

*Au menu du jour !*
1. [Qu’est-ce qu’un site web ?](#site-web) 
2. [Écrire pour le web : HTML et Markdown](#ecrire-web)  <!-- Une autre manière d'écrire -->
3. [Une solution de publication : GitHub/GitLab Pages](#publi) <img align="right" width="80" src="icono/CC0.png"> 

<!-- Objectifs de la formation : découvrir le web statique et expérimenter cette forme de publication numérique qui s’inscrit dans la philosophie de la low tech  -->

---
## Avant de commencer <!-- paginate: true -->
### Le projet STRAD

- Acronyme : Stratégie pour l’archivage durable
- Projet porté par la MSH Mondes, au sein du pôle DHUNE
- Lauréat 2024 de l’appel à initiatives pour la transition environnementale (CNRS)
- Périmètre : Université Paris Nanterre & Université Paris 1 Panthéon-Sorbonne </br>

L’ambition du projet est :
>[d’]adopter une approche basée sur l’édition de sites web statiques et accompagner les anciens sites et bases de données de projets de recherche de la MSH Mondes dans leur transition vers des solutions statiques afin de faciliter leur maintenance et archivage, et de réduire leur empreinte environnementale.

---
### La MSH Mondes et le pôle DHUNE <img align="right" width="225" src="icono/logo-MSH.jpg">

[MSH Mondes](https://mshmondes.cnrs.fr) : unité d’appui et de recherche interdisciplinaire en SHS

> La MSH met à disposition de ses usagers des instruments et des services techniques et scientifiques de haut niveau, dans les domaines de l’archivage, des bibliothèques, de la gestion des données, de l’image animée ou fixe, des humanités numériques, </br>de l’édition, de l’ingénierie documentaire, de la microscopie et l’imagerie scientifique.

[Pôle DHUNE](https://www.mshmondes.cnrs.fr/accompagnement/dhune/) : pôle Données et Humanités numériques de la MSH Mondes 

> Le pôle DHUNE apporte un appui aux communautés de recherche du périmètre de la MSH Mondes (UPN et UP1) sur le volet numérique de leurs activités et projets </br> et tout au long du cycle de vie de la donnée (gestion, traitement, ouverture...).

→ Relais locaux des infrastructures nationales (IR*) [Huma-Num](https://www.huma-num.fr) et [Progedo](https://www.progedo.fr)  <!-- données d’enquêtes -->

---
### L’IR* Huma-Num <img align="right" width="175" src="icono/logo-HN.jpg">

→ Infrastructure de recherche qui propose des services aux membres de l’enseignement supérieur et de la recherche pour les données en SHS

**Missions**
- Faciliter le « tournant numérique » de la recherche en SHS dans la production et la réutilisation de données numériques
- Proposer des services pour les données au juste niveau et au bon moment 

**Services**
- Développés par Huma-Num : [Isidore](https://documentation.huma-num.fr/isidore/) | [Nakala](https://documentation.huma-num.fr/nakala/) | [ShareDocs](https://documentation.huma-num.fr/sharedocs-stockage/)
- Additionnels : [GitLab](https://documentation.huma-num.fr/gitlab/) | [Kanboard](https://documentation.huma-num.fr/matomo/) | [Matomo](https://documentation.huma-num.fr/matomo/) | [Mattermost](https://documentation.huma-num.fr/mattermost/) 
- Tiers hébergés : [Heurist](https://documentation.huma-num.fr/heurist/) | [Opentheso](https://documentation.huma-num.fr/opentheso/) | [Stylo](https://documentation.huma-num.fr/stylo/) | [TXM](https://documentation.huma-num.fr/txm/) | [Voyant Tools](https://documentation.huma-num.fr/voyanttools/) 

Pour en savoir plus : [site web](https://www.huma-num.fr/) | [documentation](https://documentation.huma-num.fr) | [portail HumanID](https://humanid.huma-num.fr)

---
### L’atelier de la donnée de Nanterre <img align="right" width="300" src="icono/logo-ADN.png">

→ Collectif et service d’appui dédié à la question des données

> Il s’agit de mutualiser des ressources et compétences pour former, accompagner et soutenir tous ceux qui, au sein de notre communauté, sont amenés à se préoccuper de gestion des données de recherche et notamment les doctorant·es, chercheur·es et personnels d’appui.

Trois partenaires :
- **Université Paris Nanterre**, à travers son Service commun de la documentation (SCD) et la Direction de la recherche et des études doctorales (DRED)
- Maison des Sciences de l’Homme Mondes (**MSH Mondes**)
- Bibliothèque-archives-musée **La contemporaine**

Une adresse de contact : donnees-recherche@liste.parisnanterre.fr

---
<div id="site-web"/> <img align="right" width="300" src="https://thinktreemedia.in/blog/wp-content/uploads/2022/03/website-sucks.jpg">

## 1 — Qu’est-ce qu’un site web ? 

<style scoped> blockquote{font-size:23.5px} </style>

### Internet et web

**Internet** = réseau informatique mondial

> Réseau mondial de télécommunication reliant entre eux des ordinateurs ou des réseaux locaux et permettant l'acheminement de données numérisées de toutes sortes (messages électroniques, textes, images, sons, etc.). ([CNRTL](https://www.cnrtl.fr/definition/academie9/internet))

**Web** = une application d’Internet (parmi d’autres)

> Le web est un système de pages reliées par des hyperliens, qui contiennent des informations et des ressources variées. Internet est le réseau informatique sur lequel le web repose. </br>([Numerama](https://www.numerama.com/tech/1162624-quest-ce-que-le-web.html)) 

---
#### Principales composantes du web <img align="right" width="350" src="icono/web.png">

- **URI** (*Uniform Resource Identifier*) → identification
    - Identifier une ressource de manière permanente
- **URL** (*Uniform Resource Locator*) → localisation
    - Identifier une ressource du Web par son emplacement
    - Préciser le protocole internet pour la récupérer
- **HTTP** (*HyperText Transfer Protocol*) → relation
    - Permet l’échange de données sur le Web
- **HTML** (*HyperText Markup Language*) → langage de description
    - Permet d’organiser les contenus d’une page web
- **CSS** (*Cascading Style Sheets*) → langage de mise en forme
    - Permet de paramétrer l’apparence d’une page web

Pour en savoir plus : [MOOC Comprendre le web](https://openclassrooms.com/fr/courses/1946386-comprendre-le-web)

---
<img align="right" width="200" src="https://img.freepik.com/vecteurs-libre/grille-globe-internet-www_78370-2008.jpg?t=st=1729519540~exp=1729523140~hmac=e303a43bd678c59fa9f8fb78af4d682f80471b9b4f6a76662b08d9a5a79d6785&w=1800">

### Un site web <div id="site-web"/>  

- Ensemble de pages web organisées
- Document complexe contenant des éléments de navigation
- Objet éditorial (figé ou alimenté)

Plusieurs manières de créer un site web : à la main, avec un CMS (*Content Management System*) ou avec un générateur de site statique

Au départ tous les sites étaient statiques !

Mais les besoins se sont complexifiés et de nouvelles technologies sont apparues, comme les langages serveur (ex. PHP) et les CMS (ex. Wordpress)

→ Explosion des sites dynamiques, depuis les années 2000, notamment avec l’arrivée de Wordpress en 2003

---
### Site web dynamique ou statique ?
<style scoped>section{font-size:23px;} </style> 

Site web dynamique
> Un site web dynamique est conçu pour offrir une expérience interactive et personnalisée aux utilisateurs. [... ] Un site dynamique génère du contenu en temps réel en fonction de divers facteurs tels que les interactions de l’utilisateur, l’heure de la visite, ou encore les données utilisateur spécifiques stockées dans des bases de données. 

Site web statique
> Un site web statique est constitué de pages dont le contenu ne change pas automatiquement. Chaque page est codée en HTML, CSS, et parfois JavaScript, et affiche la même information à tous les visiteurs, sans intégration de bases de données ou de contenu interactif qui change en fonction de l’utilisateur.

Source / Pour en savoir plus : [Sites web statiques ou dynamiques : Quelle différence ?](https://pixlstudio.africa/sites-web-statiques-ou-dynamiques-quelle-est-la-difference/)

---
|| Site dynamique|Site statique|
|---|---|---|
|Contenu |Personnalisable selon le profil et les choix des visiteurs |Stable et identique pour tous les visiteurs, pas de personnalisation|
|Conception|Plus facile, logique du WYSWYG|Nécessite quelques compétences techniques|
|Fonctionnalités|Plus avancées|Par défaut peu avancées (mais les SSG permettent d’en ajouter)|
|Vitesse de chargement|Plus lent car il faut interroger la base de données|Plus rapide car l’intégralité du site est stockée au même endroit|
|Maintenance|À faire régulièrement|Pas ou peu de maintenance|
|Sécurité|Inférieure|Optimale|
|Suivi des modifications|Pas ou peu accessible|Simple à mettre en place avec un logiciel de gestion de versions|
|Empreinte écologique|Plus élévée (car plus d’interactions et de technologies sollicitées)| Moins élevée (low tech)|

---
<style scoped> blockquote{font-size:26px;} </style>

### Low tech
Terme qui est apparu dans les années 1970 pour désigner un concept et une philosophie de vie s’inscrivant dans une démarche minimaliste et écologique.

→ Solutions « basse technologie » (en opposition à la « _high tech_ »)

> Ensemble des technologies utiles, durables et économiques visant la sobriété énergétique et matérielle. ([Hello Carbo](https://www.hellocarbo.com/blog/reduire/low-tech/))

Trois grands principes :
1. **Utilité** : réponse à un besoin réel et pertinent, usage agréable et autonome…
2. **Accessibilité** : coût abordable, facilité d’utilisation, normes partagées…
3. **Durabilité** : réutilisation, réparation, recyclage, faible niveau de dépendance…

---
<style scoped> img {margin-top:-10px; margin-left:-20px} </style>
[![width:1125](icono/low-tech.jpeg)](https://fr.wikipedia.org/wiki/Fichier:Innovation_low-tech.jpg)

---
### Exemples de sites statiques

- Cours Hugo (R. Witz) `(Hugo | Beautiful Hugo)` [code source](https://gitlab.huma-num.fr/misha/demo/hugo) | [site](https://misha.gitpages.huma-num.fr/demo/hugo/)
- Cours Python (P. Poulain, P. Fuchs) `(MKDocs | Material)` → [code source](https://github.com/bioinfo-prog/cours-python) | [site web](https://python.sdv.u-paris.fr)
- Documentation Huma-Num `(MKDocs | Read the docs)` → [site web](https://documentation.huma-num.fr)
- *Journal of Open Source Software* `(Sphinx | Read the docs)` → [code source](https://github.com/openjournals/joss) | [site web](https://joss.theoj.org)
- MOOC Impacts environnementaux du numérique (INRIA) `(MKDocs)` → [site web](https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/index.html)
- Présentation revealJS (K. Gautreau) `(revealJS)` → [code source](https://gitlab.com/formations-kgaut/poc-presentation) | [site web](https://formations-kgaut.gitlab.io/poc-presentation)
- Projet Revue 2.0 `(Jekyll)` → [code source](https://gitlab.huma-num.fr/ecrinum/revue20/revue20-org) | [site web](https://www.revue20.org)
- Projet Rzine `(Hugo)` → [code source](https://gitlab.huma-num.fr/rzine/site) | [site web](https://rzine.fr)
- *Programming Historian* (revue) `(Jekyll)` → [code source](https://github.com/programminghistorian/jekyll) | [site web](http://programminghistorian.org)
- Site personnel de Luc Didry (Framasoft) `(Hugo)` → [code source](https://framagit.org/luc/luc.frama.io/) | [site web](https://luc.frama.io)
- Tutoriel Markdown (A. Perret) `(HTML | CSS | JS)` → [code source](https://github.com/commonmark/commonmark-web/tree/gh-pages/help) | [site web](https://www.arthurperret.fr/tutomd)

---
<style scoped> img {margin-top: 10px; margin-left:-30px} </style>

## 2 — Écrire pour le web <div id="ecrire-web"/>

### Le langage du web : HTML <img align="right" width="550" src="icono/html.png">

[HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language) (*HyperText Markup Language*)
- <!--LE--> Langage de structuration du web 
- Langage standardisé, spécifié par le W3C (World Wide Web Consortium)
- Souvent lié à une feuille de style [CSS](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade)</br> (langage de présentation des pages web)
- Tout contenu doit être dans un élément (balise) `<element> Contenu </element>`

Beaucoup de ressources pour apprendre !
- Exemple : [w3schools](https://www.w3schools.com/html/)

**EXERCICE 1** : reproduire le même code dans l’éditeur [w3schools.com](https://www.w3schools.com/html/tryit.asp?filename=tryhtml_default)

---
### Une alternative pour l’écriture web : Markdown <img align="right" width="150" src="icono/logo-MD.png">

<style scoped> section{font-size:25px} </style>

Markdown est un **langage de balisage léger** facile à lire et à écrire </br>crée en 2004 par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) (avec l’aide d’[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz) pour la syntaxe) 

Philosophie de Markdown ([selon son créateur](https://daringfireball.net/projects/markdown/syntax)) :
> Un document formaté en Markdown doit pouvoir être publié tel quel, en texte brut, sans donner l’impression d’avoir été structuré par des balises ou des instructions de formatage.
>
>La syntaxe Markdown est conçue dans un seul but : être utilisée comme **format d'écriture pour le web**.

Avantages :
- **Simplicité** : facile à apprendre, lire et écrire ; à copier, partager et conserver 
- **Portabilité** : multiplateforme / support / programme ; document pivot convertissable 
- **Modernité** : écriture numérique et académique ; minimalisme et sobriété numérique

---
<style scoped> p{font-size:24px;}</style> 

### Écrire en Markdown

|Markdown|HTML|Rendu|
|-|-|-|
|`# Titre 1`|`<h1>Titre1</h1>`|Titre 1|
|`*italique*` ou `_italique_`|`<em>italique</em>`|*italique*|
|`**gras**`|`<strong>gras<strong>`|**gras**|
|`***gras italique***`|`<strong><em>gras italique</em></strong>`|_**gras italique**_|
|`XIX^e^` |`XIX<sup>e</sup>` |XIX<sup>e</sup> |
| `H~2~O`|`H<sub>2</sub>O` |H<sub>2</sub>O |
|`[hyperlien](url)` |`<a href="url">hyperlien</a>` |[hyperlien]()|
|`- Liste`| `<ul><li>Liste</li></ul>` | - Liste|
|`![légende](image.jpg)`|`<img src="image.jpg" alt="légende">`|![width:50](icono/picto-img.png)|

Grande variété d’éditeurs Markdown (à télécharger, en ligne, intégré dans d'autres outils..)

---
<style scoped> footer{font-size:0px;} </style>

![bg 88%](icono/MD-apercu.png)

---
<style scoped> section{font-size:25px;} img {margin-right: 40px;}</style> 

### Un éditeur en ligne : HedgeDoc <img align="left" width="480" src="icono/HedgeDoc-apercu2.png">

&rarr; **Outil de prise de notes** collaborative simultanée 

Instance test officielle : https://demo.hedgedoc.org
Instance gouv.fr : https://pad.numerique.gouv.fr

**Principales fonctionnalités** : 

–  Espaces de travail : lecture, écriture, les deux
–  Thèmes : jour ou nuit
–  Barre d’aide à la rédaction (et bouton aide) 
–  Aperçu des personnes connectées
–  Gestion fine des permissions 
–  Historique des versions
–  Page de visualisation partageable
–  Mode présentation (avec `reveal.js`) 
–  Exports en MD et HTML

---
<style scoped> img{display:block;margin: 0 auto;}</style> 

### Découvrir Markdown
**EXERCICE 2** : [tutoriel interactif](https://www.arthurperret.fr/tutomd/) du CommonMark (traduction d’Arthur Perret)

[![width:1000](icono/MD-tuto.png)](https://www.arthurperret.fr/tutomd)

---
<style scoped> section{font-size:25.5px;}</style> 

## 3 — Une solution de publication : GitHub/GitLab Pages <div id="publi"/>

### Le protocole Git
<img align="right" width="200" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1024px-Git-logo.svg.png?20160811101906">

&rarr; **Protocole d’échange, de synchronisation et de versionnement** de fichiers textes 
- Créé en 2005 par Linus Torvalds, le créateur de Linux
- Très utile pour garder un historique complet des modifications apportées </br>et pour le travail collaboratif et simultané
- Fichiers synchronisés en ligne, dans des éditeurs de texte (ex. Atom ou VS Code) </br>ou des logiciels de versionnement (ex. GitHub Desktop)

Quels fichiers ? <!-- principalement -->
- Des codes sources (ex. au format R ou PY)
- Des fichiers de documentation ou de description (ex. au format HTML, MD, IPYNB)
- Stockage possible des autres types de fichiers (mais sans versionnement)

---
### Les plateformes Git <img align="right" width="440" src="icono/GitLab-apercu.png">

Git ≠ GitHub/GitLab/Framagit : </br>&rarr; plateformes collaboratives de gestion de projet et de partage de codes basées sur le protocole Git 
- Depuis 2019, [instance GitLab d’Huma-Num](gitlab.huma-num.fr) accessible à partir du [portail HumanId](https://humanid.huma-num.fr) 

Trois niveaux de visibilité des contenus : 
- Public
- Public avec authentification 
- Privé (accès uniquement aux membres du projet)

Regroupement possible de plusieurs projets au sein d’un groupe (ou d’une organisation)

---
### Partager vos fichiers de documentation sur GitLab ou GitHub

**Fichier « Lisez-moi »** (*readme*) :
- Généralement à la racine du projet
- Informations générales sur le projet, notamment les objectifs et la méthodologie
- Explications sur l’organisation du répertoire (*repository*)

**Fichier « Licence »** : informations sur la licence utilisée

**Tickets** (*issues*) :
- Permettent de faire remonter des bugs, des questions, des retours d’expérience, etc.
- S’affichent sous forme de liste, tableau ou jalons
- Outil pour la collaboration et la gestion de projet

**Répertoire de documentation** (*wiki*) : documentation interne au sein d’un groupe/projet

--- 
<style scoped> footer{font-size: 0px;} </style> 
![bg 78%](icono/GitLab-apercu-tickets.png)

---
### Créer un site web avec la fonctionnalité `Pages`

[GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) (ou [GitHub Pages](https://pages.github.com)) : fonctionnalité qui permet de générer et de publier, gratuitement et en continu, un site statique associé à un répertoire GitLab (ou GitHub) et hébergé sur les serveurs de GitLab (ou GitHub) – en France pour l’instance Huma-Num

**Avantages**
- Gain de temps et de ressources
- Nombre de sites hébergés non limité (avec GitLab)
- Adapté aux sites minimalistes ou principalement textuels

**Fonctionnement**
- Alimentation du site avec des fichiers textes `md` ou `html`
- Gestion des sauvegardes/versions avec GitLab ou GitHub (impératif)
- Publication avec des générateurs de site statique (facultatif) 

---
<style scoped> section{font-size: 24.5px;} </style> 

### Les générateurs de site statiques <!-- Static Site Generator/  SSG --><img align="right" width="350" src="https://docs.gitlab.com/ee/user/project/pages/img/ssgs_pages_v11_3.png">

= outil qui permet de créer des sites web composés de pages statiques, généralement au format HTML (ou MD)

<!-- Contrairement aux systèmes de gestion de contenu (CMS) dynamiques, les générateurs de sites statiques produisent des fichiers HTML, CSS et JavaScript prêts à être servis directement par un serveur web, sans nécessiter de traitement côté serveur au moment de la requête. -->

Fonctionnement ([Wikipédia](https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_site_statique))
1. **Création du contenu** – L’utilisateur crée le contenu du site, souvent dans un langage de balisage léger comme Markdown.
2. **Design des pages** – Des modèles sont créés pour définir l’architecture et la mise en page du site, par exemple avec des feuilles de style CSS.
3. **Configuration** – L’utilisateur configure les paramètres du site, tels que les métadonnées et la structure de navigation.
4. **Génération** – Le générateur compile le contenu, les modèles et la configuration pour produire des fichiers HTML statiques.
5. **Déploiement** – Les fichiers générés sont téléchargés sur un serveur web ou une plateforme d’hébergement.

--- 
<style scoped> footer{font-size: 0px;} </style> 
[![bg 80%](icono/SSG-pages.png)](https://gitlab.com/pages)

---
<style scoped> img {margin-top:-10px; margin-right:20px;}</style>
<img align="right" width="385" src="icono/GitLab-pages-new.png">

### Place à la pratique !

EXERCICE 3 : créer son premier site statique
→ Deux possibilités : avec GitLab Pages ou GitHub Pages 
</br>

#### Avec le GitLab d’Huma-Num

**1.**  Dans GitLab, **créer un nouveau projet**, </br>      à partir d’un modèle, par ex.`Pages/Plain HTML`

**2.**  Compléter les informations : 

–  Nom du projet
–  Diminutif (*slug*) dans l’adresse URL
–  Description du projet 
–  Niveau de visibilité public

---
<img align="right" width="450" src="icono/GitLab-cv.png">

**3.**  Une fois le répertoire créé, ajouter le fichier       qui constituera votre site web :

–  Télécharger le [fichier HTML modèle](https://gitlab.huma-num.fr/siteStatique/modele-cv/-/blob/master/public/index.html?ref_type=heads).
–  Dans le dossier `Public` du projet <!--que vous venez de créer-->créé, </br>    cliquer sur le fichier `index.html`.
–  Cliquer sur `Remplacer` et téléverser le modèle.

**4.**  Modifier le fichier HTML à votre guise.

**5.**  Attendre que GitLab exécute le pipeline </br>      et génére votre site. 

Une fois le pipeline terminé, accéder à votre site via l’URL basée sur le modèle suivant : 
`[nom-utilisateur].gitpages.huma-num.fr/[nom-projet]`

---
<style scoped> img{margin-right:30px;} </style>

#### Avec GitHub  <img align="left" width="425" src="icono/GitHub-new.png">

**1.**  Dans GitHub, **créer un nouveau répertoire** vide </br>(`+`puis `New repository`) 

**2.**  Compléter les informations :

– Nom du projet (`Repository name`)
– Description du projet
– Niveau de visibilité `public` (obligatoire)

**3.**  Une fois le répertoire créé, ajouter le fichier </br>      qui constituera votre site web :

–  Télécharger le [fichier HTML modèle](https://gitlab.huma-num.fr/siteStatique/modele-cv/-/blob/master/public/index.html?ref_type=heads)
–  Téléverser le modèle 
–  Remplacer son titre pour `index.html`

---
<style scoped> img {margin-top: -20px; margin-right:10px;} section{font-size:25px;} </style>

<img align="right" width="420" src="icono/GitHub-config.png">

**4.**  Déployer le site de votre dépôt :

–  Dans Réglages (*Settings*), cliquer sur `Pages` et choisir</br>     `GitHub Actions` comme source de déploiement.

–  Selon la nature des fichiers déposés, choisir l’action</br>     (*workflow*) correspondante, ici `Static HTML`.

–  Valider la création du pipeline (`static.yml`)</br>      en cliquant sur `Commit changes`.

**5.**  Laisser GitHub exécuter le pipeline et génèrer le site. 

Une fois le pipeline terminé, accéder au site :

– Via l’URL`https://[username].gitlab.io/[project-name]`
– En cliquant sur le résultat dans `Deployments` (à droite)

---
![width:1100](icono/merci.png)